// service layer 
// 	ReaderServiceClient.java 

public class ReaderServiceClient {
	private ApplicationContext ctx = null;
	
	private ReaderService service = null;
	// ReaderServiceClient only knows ReaderService.
	// ReaderServiceClient does not know IReader, etc... 
	
	public ReaderServiceClient() {
		ctx = new ClassPathXmlApplicationContext("basics-reader-beans.xml");
		service = (ReaderService) ctx.getBean("readerService");
	}
	
	private String fetchData() {
		return service.fetchData();
	}
	
	public static void main(String[] args) {
		ReaderServiceClient client = new ReaderServiceClient();
		System.out.println("Got data using ReaderService: " + client.fetchData());
	}
}

/* basics-reader-beans.xml

<beans>
	<bean name="readerService"
	class="com.madhusudhan.jscore.basics.service.ReaderService">
		<constructor-arg ref="reader" />
	</bean>
	
	<bean name="reader"
	class="com.madhusudhan.jscore.basics.readers.FileReader">
		<constructor-arg
		value="src/main/resources/basics/basics-trades-data.txt" />
	</bean>
</beans>

*/