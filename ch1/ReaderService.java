// service 
//		ReaderService.java 

public class ReaderService {
	
	private IReader reader = null;
	
	public ReaderService(IReader reader) {
		this.reader = reader;
	}
	
	public String fetchData() {
		return reader.read();
	}
}

