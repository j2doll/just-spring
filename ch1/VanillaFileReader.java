//
// VanillaFileReader.java

public class VanillaFileReader {
	private StringBuilder builder = null;
	
	private Scanner scanner = null;
	
	public VanillaFileReader(String fileName) throws FileNotFoundException {
		scanner = new Scanner(new File(fileName));
		builder = new StringBuilder();
	}
	
	public String read() {
			while (scanner.hasNext()) {
			builder.append(scanner.next());
			builder.append(",");
		}
		return builder.toString();
	}
}
