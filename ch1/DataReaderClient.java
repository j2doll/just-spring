// 
// DataReaderClient.java 

public interface IReader {
	public String read();
}

/*
public class XXXReader implements IReader {
	public String read() {
		// implement read()
	}
}
*/ 

public class DataReaderClient {
	private IReader reader = null;
	
	private static String fileName
		= "src/main/resources/basics/basics-trades-data.txt";
		
	public DataReaderClient(IReader reader) {
		this.reader = reader;
	}
	
	private String fetchData() {
		return reader.read();
	}
	
	public static void main(String[] args) {
		DataReaderClient dataReader = new DataReaderClient();
		System.out.println("Got data using no-spring: " +
		dataReader.fetchData());
	}
}