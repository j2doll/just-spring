// Introduction to IoC(Inversion of Control) 
//		DecoupledDataReaderClient.java

public class DecoupledDataReaderClient {
	private IReader reader = null;
	
	private ApplicationContext ctx = null;
	
	public DecoupledDataReaderClient() {
		ctx = new ClassPathXmlApplicationContext("basics-reader-beans.xml");
	}
	
	private String fetchData() {
		reader = (IReader) ctx.getBean("reader");
		// get 'reader' from *.xml. operator 'new' is not used. 
		return reader.read();
	}
	
	public static void main(String[] args) {
		DecoupledDataReaderClient client = new DecoupledDataReaderClient();
		System.out.println(
			"Using Decoupled Data Client, Got data: " + client.fetchData()
		);
	}
}

/* basics-reader-beans.xml

	<?xml version="1.0" encoding="UTF-8"?>
	<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation= "http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans.xsd">
	
		<bean name="reader" class="com.madhusudhan.jscore.basics.FileReader">
			<constructor-arg value
			="src/main/resources/basics/basics-trades-data.txt" />
		</bean>
		
	</beans>
*/