// PersonClient.java

public class PersonClient {
	
	private static ApplicationContext context = null;
	
	public PersonClient() {
		context = new ClassPathXmlApplicationContext("ch2-spring-beans.xml");
		// read beans from a xml file.
	}
	
	public String getPersonDetails() {
		Person person = (Person) context.getBean("person"); // get 'person'
		return person.getDetails();
	}
}

/*
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation= "http://www.springframework.org/schema/beanshttp://www.springframework.org/schema/beans/spring-beans.xsd">

	<bean name="person" class="com.madhusudhan.jscore.beans.Person">
		<constructor-arg value="Madhusudhan" />
		<constructor-arg value="Konda" />
		<property name="age" value="99" />
		<property name="address" ref="address" />
	</bean>
	
	<bean name="address" class="com.madhusudhan.jscore.beans.Address">
		<property name="doorNumber" value="99" />
		<property name="firstLine" value="Rainbow Vistas" />
		<property name="secondLine" value="Kukatpally, Hyderabad" />
		<property name="zipCode" value="101010" />
	</bean>
	
</beans>

*/
