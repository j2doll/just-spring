// ReservationManager.java

@Component
public class ReservationService {
	public void doReserve(ReservationMessage msg) {
		// ... 
	}
}

public class ReservationManager {

	@Autowired
	private ReservationService reservationService = null;
	
	public void process(Reservation r) {
		reservationService.reserve(r);
	}
}

/*
<beans xmlns="http://www.springframework.org/schema/beans"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:context="http://www.springframework.org/schema/context"
xsi:schemaLocation="http://www.springframework.org/schema/beans
http://www.springframework.org/schema/beans/spring-beans.xsd
http://www.springframework.org/schema/context
http://www.springframework.org/schema/context/spring-context.xsd">

<context:annotation-config />

<!-- reserved service -->

<bean name="resSvcABC" class
="com.madhusudhan.jscore.fundamentals.annotations.ReservationService" />

<bean name="reservationManager" class
="com.madhusudhan.jscore.fundamentals.annotations.ReservationManager" />
</beans>

*/