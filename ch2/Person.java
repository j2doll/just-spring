// Person.java

public class Person {

	private int age = 0;
	private String firstName = null;
	private String lastName = null;
	private Address address = null;
	
	public Person(String fName, String lName) {
		firstName = fName;
		lastName = lName;
	}

	// getter & setter of age 
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	// getter & setter of address 	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

	public String getDetails(){
		return firstName + " " + lastName + " is " + getAge() + " old and lives at " + getAddress();
	}
}


